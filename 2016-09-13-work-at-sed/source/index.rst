
.. Activites SED 2016 slides file, created by
   hieroglyph-quickstart on Tue Sep  6 17:36:19 2016.

==================
Activités SED 2016
==================
:Auteur: Mauricio DIAZ



À propòs de moi
========================

- au SED depuis octobre 2014
- thèse soutenue en 2011 à l'INRIA Grenoble
- 2011 - 2014 Ingenieur de recherche dans le privé
- 2014 - 2015 Soutien 80% chez Willow
- 2014 - 2015 Référent local de la plateforme CI

Activités 2016-2017
====================

**Entre septembre et décembre 2015 :**

- 80% chez Willow
- 20% de temps de service

**Entre janvier et mars 2016 :**

- 70% chez Willow
- 10% chez Aramis
- 20% de temps de service

**Depuis avril 2016 :**

- 80% chez Willow
- 20% de temps de service

Aramis
===============

Quel format pourrait être utilisé pour gérer les 
entrées du logiciel *Deformetrica* ?

Problèmatique :

.. code-block:: bash

  $ sparseMatching2 paramSub1.xml paramCurvesSub1.xml skull_Sub1.vtk 
  paramSub2.xml paramCurvesSub2.xml skull_Sub2.vtk ... paramSubN.xml 
  paramCurvesSubN.xml skull_SubN.vtk skull_subFinal.vtk

- La solution proposée : fichier JSON.
- Utilisation de la bilbiotechque `cereal <http://uscilab.github.io/cereal/>`_ : serialization de fichiers, C++11, header-only.
- Un exemple à été proposé dans depôt du projet. 



20% de temps de service
========================

- Formation C++ (7 et 8 décembre)
- Suivi d'ingénieurs ADT
- Documentation du cluster de calcul RIOC (usage gpu)



Formation C++
==============

- 7 et 8 decembre
- Malgré un sondage repondu par 35 personnes potentiellement interesées, 
  seulement 4 inscrits. 
- *Deux sessions :*
  
  * Procedural programming 
  * Object oriented programming

- Supports disponibles 

.. code-block:: bash

  $ 04_training/2015_langages/introductionC++11/

Suivi des ingénieurs ADT
=========================

- Ange TOULOUGOUSSOU

  * Équipe : *Alpines*
  * Projet ADT : DirPrePack
  * Responsable : Laura Grigori
  * Février 2015 - Janvier 2017

- Michael BACCI

  * Équipe : *Aramis*
  * Projet ADT : Clinica
  * Responsable : Stanley Durrleman
  * Janvier 2016 - Décembre 2017

  
Documentation du RIOC
======================

- Sphinx
  Documentation sur un serveur *readthedocs.io* (mise en place par David et Cédric).
- Hebergé sur *bitbucket.org*.
- Example pas à pas : comment faire l'implentation d'une operation entre matrices
  en utiisant le GPU (CUDA).
- `Utilisation du GPU <http://rioc.readthedocs.io/en/master/gpu.html>`_


Willow
======

- Responsable d'EPI : Jean Ponce
- Responsable du projet : Jean Ponce
- Objetif : outils pour la reconstruction 3D multivues


INRIAMVS
=========

- Groupe d'outils pour la reconstruction (pasive) 3D multivues.

*4 grandes étapes :*

- Calibration de cameras
- Obtention d'un nuage de points dense
- Maillage de ces points
- Texturisation

INRIAMVS (continuation)
========================
Images d'entrée

.. image::  /_static/kermit000.jpg

INRIAMVS (continuation)
========================
Images d'entrée

.. image::  /_static/kermit001.jpg

INRIAMVS (continuation)
========================
Images d'entrée

.. image::  /_static/kermit002.jpg

INRIAMVS (continuation)
========================
Images d'entrée

.. image::  /_static/kermit003.jpg

INRIAMVS (continuation)
========================
Nuage de points dense

.. image::  /_static/points.png
   :scale: 75 %

INRIAMVS (continuation)
========================
Maillage

.. image::  /_static/mesh.png
   :scale: 75 %

INRIAMVS (continuation)
========================
Texturisation

.. image::  /_static/meshText.png
   :scale: 75 %




INRIAMVS (continuation)
========================

- *Calibration de cameras (bundler)*
- *Obtention d'un nuage de points dense (pmvs)*
- **-> Maillage de ces points <-** 
- Texturisation

Maillage
==========

- Récriture du module (architechture du logiciel)
  (Par example, utilisation de boost program options pour parser les entrées,
  definition de clases, etc).
- Tetrahédralisation de Delaunay sur le nuage de points
- Méthode simple, surface des tetrahèdres


Maillage
==========

- Méthode "weakly--supported surfaces"
- Ameliorer des regions où la densité de points est faible
- Équation pour penaliser/favoriser certaines relations entre les tetrahèdres :
 .. image:: /_static/equation.png

- **Lanceur de rayons (refait)**, étude de la librarie CGAL.  

Maillage
=========

- Réglage des paramétres de la reconstruction.
- Dataset de plus de 500 images
- ~ 3 millons de points
- ~ 6 millons de triangles
- Implementation du parametre photomètrique 
  


D'autres activités
===================

- Création de paquets Conda pour l'installation d'OpenMVG
- Présentation du projet pendant la journée de nouveaux arrivants 2015
- Migration vers git du projet
  
===============
Merci !
===============

